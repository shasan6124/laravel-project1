<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', function () {
    return view('components.frontend.partials.contents');
});

Route::get('/cart', function () {
    return view('components.frontend.common.cart');
});

Route::get('/checkout', function () {
    return view('components.frontend.common.checkout');
});

Route::get('/order', function () {
    return view('components.frontend.common.order');
});
Route::get('/product', function () {
    return view('components.frontend.common.product-simple');
});

Route::get('/shop', function () {
    return view('components.frontend.common.shop-classic-filter');
});

Route::get('/login', function () {
    return view('components.frontend.common.login');
});

Route::get('/categoris', function () {
    return view('components.frontend.common.categoris');
});


Route::get('/backend', function () {
    return view('components.backend.layouts.master');
});