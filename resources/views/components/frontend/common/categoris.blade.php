<x-frontend.layouts.master>
		<main class="main">
			<div class="page-content mb-10 pb-2">
				<div class="container">
					<nav class="breadcrumb-nav">
						<ul class="breadcrumb breadcrumb-lg">
							<li><a href="demo2.html"><i class="d-icon-home"></i></a></li>
							<li>Categories</li>
						</ul>
					</nav>
					<div class="banner shop-banner"
						style="background-image: url('ui/frontend/images/demos/demo2/shop_banner.jpg'); background-color: #edeff0;">
						<div class="banner-content w-100 pl-3 text-right text-uppercase">
							<h4 class="banner-subtitle font-weight-bold text-uppercase mb-2">24 Hours Only</h4>
							<h1 class="banner-title mb-3 text-uppercase font-weight-normal">Flash <strong>Sale</strong>
							</h1>
							<div class="banner-price-info mb-3 text-dark"><strong>Up to 70</strong>% Discount</div>
							<p class="d-inline-block mb-6 font-primary text-secondary text-uppercase bg-dark">
								Promocode:<strong class="text-normal"> Riode 20</strong></p><br />
							<a href="#" class="btn btn-link btn-underline">Shop now<i
									class="d-icon-arrow-right"></i></a>
						</div>
					</div>
					<div class="toolbox-wrap">
						<aside class="sidebar sidebar-fixed shop-sidebar closed">
							<div class="sidebar-overlay"></div>
							<a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
							<div class="sidebar-content">
								<div class="mb-0 mb-lg-4">
									<div class="filter-actions">
										<a href="#"
											class="sidebar-toggle-btn toggle-remain btn btn-sm btn-outline btn-primary btn-rounded">Filter<i
												class="d-icon-arrow-left"></i></a>
										<a href="#" class="filter-clean text-primary">Clean All</a>
									</div>
									<div class="row cols-lg-4">
										<div class="widget">
											<h3 class="widget-title">Size</h3>
											<ul class="widget-body filter-items">
												<li><a href="#">Extra Large</a></li>
												<li><a href="#">Large</a></li>
												<li><a href="#">Medium</a></li>
												<li><a href="#">Small</a></li>
											</ul>
										</div>
										<div class="widget">
											<h3 class="widget-title">Color</h3>
											<ul class="widget-body filter-items">
												<li><a href="#">Black</a></li>
												<li><a href="#">Blue</a></li>
												<li><a href="#">Brown</a></li>
												<li><a href="#">Green</a></li>
											</ul>
										</div>
										<div class="widget">
											<h3 class="widget-title">Brand</h3>
											<ul class="widget-body filter-items">
												<li><a href="#">Cinderella</a></li>
												<li><a href="#">Comedy</a></li>
												<li><a href="#">SkillStar</a></li>
												<li><a href="#">SLS</a></li>
											</ul>
										</div>
										<div class="widget price-with-count filter-price">
											<h3 class="widget-title">Price</h3>
											<ul class="widget-body filter-items">
												<li class="active"><a href="#">All</a><span>(10)</span></li>
												<li><a href="#">$0.00 - $100.00</a><span>(1)</span></li>
												<li><a href="#">$100.00 - $200.00</a><span>(9)</span></li>
												<li><a href="#">$200.00+</a><span>(3)</span></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</aside>
						<div class="toolbox sticky-content sticky-toolbox fix-top">
							<div class="toolbox-left">
								<a href="#"
									class="toolbox-item left-sidebar-toggle btn btn-outline btn-primary btn-icon-left font-primary btn-rounded"><i
										class="d-icon-filter-2"></i>Filter</a>
							</div>
							<div class="toolbox-right">
								<div class="toolbox-item toolbox-sort select-box">
									<label>Sort By :</label>
									<select name="orderby" class="form-control">
										<option value="default" selected="selected">Default sorting</option>
										<option value="popularity">Sort by popularity</option>
										<option value="rating">Sprt by average rating</option>
										<option value="date">Sort by latest</option>
										<option value="price-low">Sort by price: low to high</option>
										<option value="price-high">Sort by price: high to low</option>
									</select>
								</div>
								<div class="toolbox-item toolbox-layout">
									<a href="shop-list-mode.html" class="d-icon-mode-list btn-layout"></a>
									<a href="shop.html" class="d-icon-mode-grid btn-layout active"></a>
								</div>
							</div>
						</div>
					</div>
					<div class="row cols-2 cols-sm-3 cols-md-4 product-wrapper">
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/1.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">new</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Women</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Comfortable Brown Scart</a>
									</h3>
									<div class="product-price">
										<span class="price">$140.00</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 12 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/2.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-sale">27% off</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Clothing</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Cotton-padded Clothing</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$125.99</ins><del class="old-price">$160.99</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:60%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 8 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/3.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Shoes</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Season Sports Sneaker</a>
									</h3>
									<div class="product-price">
										<span class="price">$78.64</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:40%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 2 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/4.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">New</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Clothing</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Women Red Fur Overcoat</a>
									</h3>
									<div class="product-price">
										<span class="price">$184.00</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:80%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/5.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Women</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Hempen Hood a Mourner</a>
									</h3>
									<div class="product-price">
										<span class="price">$93.24</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:40%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 9 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/6.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">New</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Bags & Backpacks</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Women's Season Handbag</a>
									</h3>
									<div class="product-price">
										<span class="price">$61.35</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:80%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 63 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/7.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-sale">13% off</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Shoes</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Converse Blue Trainaing Shoes</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$347.23</ins><del class="old-price">$386.23</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:40%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 14 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/8.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick
											View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Bags & Backpacks</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">A Dress-Suit Valise</a>
									</h3>
									<div class="product-price">
										<span class="price">$78.23</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 53 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/9.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-sale">35% Off</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Accessories</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Hand Electric Cell</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$26.00</ins><del class="old-price">$38.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/10.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">new</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Shoes</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Season Sports Blue Sneaker</a>
									</h3>
									<div class="product-price">
										<span class="price">$119.58</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:80%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 52 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/11.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>

									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Women</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Hempen Hood a Mourner</a>
									</h3>
									<div class="product-price">
										<span class="price">$43.26</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:40%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 62 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/12.jpg" alt="Blue Pinafore Denim Dress"
											width="280" height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-label-group">
										<label class="product-label label-new">new</label>
									</div>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">Bags & Backpacks</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Women’s Fashion Handbag</a>
									</h3>
									<div class="product-price">
										<span class="price">$184.00</span>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:80%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 23 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product shadow-media">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/13.jpg" alt="product" width="280"
											height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">categories</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Men's Fashion Shirt</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product shadow-media">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/14.jpg" alt="product" width="280"
											height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">categories</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Black Fashion T Shirt</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product shadow-media">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/15.jpg" alt="product" width="280"
											height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">categories</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Dark Blue Suede School Bag</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
						<div class="product-wrap">
							<div class="product shadow-media">
								<figure class="product-media">
									<a href="demo2-product.html">
										<img src="ui/frontend/images/demos/demo2/products/16.jpg" alt="product" width="280"
											height="315" style="background-color: #f2f3f5;" />
									</a>
									<div class="product-action-vertical">
										<a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
											data-target="#addCartModal" title="Add to cart"><i
												class="d-icon-bag"></i></a>
										<a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
												class="d-icon-heart"></i></a>
									</div>
									<div class="product-action">
										<a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
									</div>
								</figure>
								<div class="product-details">
									<div class="product-cat">
										<a href="demo2-shop.html">categories</a>
									</div>
									<h3 class="product-name">
										<a href="demo2-product.html">Women's Fashion Summer Dress</a>
									</h3>
									<div class="product-price">
										<ins class="new-price">$98.00</ins><del class="old-price">$210.00</del>
									</div>
									<div class="ratings-container">
										<div class="ratings-full">
											<span class="ratings" style="width:100%"></span>
											<span class="tooltiptext tooltip-top"></span>
										</div>
										<a href="demo2-product.html" class="rating-reviews">( 6 reviews )</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<nav class="toolbox toolbox-pagination justify-content-center">
						<ul class="pagination">
							<li class="page-item disabled">
								<a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
									aria-disabled="true">
									<i class="d-icon-arrow-left"></i>Prev
								</a>
							</li>
							<li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
							<li class="page-item"><a class="page-link" href="#">2</a></li>
							<li class="page-item"><a class="page-link" href="#">3</a></li>
							<li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
							<li class="page-item">
								<a class="page-link page-link-next" href="#" aria-label="Next">
									Next<i class="d-icon-arrow-right"></i>
								</a>
							</li>
						</ul>
					</nav>
				</div>
			</div>
			
		</main>
</x-frontend.layouts.master>